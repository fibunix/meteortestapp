if(Meteor.isClient){

   Template.people.helpers({
      people : function(){
         return People.find({team_id:null});
      },
      places : function(){
         return Places.find({});
      }
   });

   Template.people.events({

   });


   Template.person.events({
      'dblclick .card' : function(e){
         if( ! $(e.currentTarget).hasClass('taken') ) {
            $(e.currentTarget).addClass('taken')
            Session.set('person_id', this._id);
            console.log('taken');
         } else {
            $(e.currentTarget).removeClass('taken')
            Session.set('person_id', null);
            console.log('free');
         }

      }
   })



   Template.person.onRendered(function(){
   });
}
